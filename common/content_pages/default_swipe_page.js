import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  Alert,
  ScrollView, View,
  Dimensions
} from 'react-native';

import styles from '../styles'
import Swiper from 'react-native-swiper';
import Orientation from 'react-native-orientation'

let window = Dimensions.get('window')

export default class DefaultSwipePage extends Component {
  constructor(_){
    super(_)
  }
  render() {

    var next = <Text style={{color: 'white', fontSize: 50, paddingRight: 0, textShadowColor: '#000', textShadowOffset: {width: 2, height: 2}, textShadowRadius: 5}}>›</Text>;
    var previous = <Text style={{color: 'white', fontSize: 50, paddingRight: 0, textShadowColor: '#000', textShadowOffset: {width: 2, height: 2}, textShadowRadius: 5}}>‹</Text>;
    return (
          <Swiper
            showsPagination={true}
            showsButtons={true}
            bounces={true}
            loop={false}
            dotStyle={{marginBottom: 0}}
            activeDotStyle={{marginBottom: 3}}
            dotColor='white'
            activeDotColor='yellow'
            nextButton={next}
            prevButton={previous}
          >
            {this.props.children}
          </Swiper>
    );
  }
}
