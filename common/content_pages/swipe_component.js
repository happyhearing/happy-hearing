import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  Alert,
  ScrollView,
  View,
  Image
} from 'react-native';

import SwipeCard from '../../common/content_pages/swipe_card'
import VideoPlayer from '../../common/content_pages/video_player'
import styles from '../styles'

function HeaderSection(props) {
    return (
        <View style={[styles.swipeContentSpacing, styles.swipeContentHeader, props.viewStyle]}>
            {props.children}
        </View>
    );
}

function MiddleSection(props) {
    return (
        <View style={[styles.swipeContentSpacing, styles.swipeContentHeader]}>
            {props.children}
        </View>
    );
}

function ImageSection(props) {
    return (
        <View style={[styles.swipeImageContentSpacing, styles.swipeContentImage]}>
            {props.children}
        </View>
    );
}

function VideoPart(props) {
    return (
        <View style={[styles.swipeContentSpacing, styles.swipeContentVideo]}>
            {props.children}
        </View>
    );
}

function FooterSection(props) {
    return (
        <View style={[styles.swipeContentSpacing, styles.swipeContentFooter, props.footerStyle]} >
            {props.children}
        </View>
    );
}


export function BasicText(props) {
    return (
        <Text style={[styles.contentText, styles.swipeContentText, props.textStyle]}>
            {props.content}
        </Text>
    );
}

export function MultiLineText(props) {
    return (
        <Text style={[styles.contentText, styles.swipeContentText, props.textStyle]}>
            {props.firstLine}{"\n"}{props.secondLine}
        </Text>
    );
}

export function Section(props) {
    return (
        <SwipeCard>
            <HeaderSection>
                <BasicText content={props.headerText} />
            </HeaderSection>
            <ImageSection>
                {props.middleContent}
                <Image style={[styles.image, props.imageStyle]} source={props.imageSource} />
                <BasicText content={props.middleText} />
            </ImageSection>
            <FooterSection footerStyle={props.footerStyle}>
                {props.footerContent}
            </FooterSection>
        </SwipeCard>
    );
}

export function SimpleSection(props) {
    return (
        <SwipeCard>
            <MiddleSection>
                <BasicText content={props.text} />
                {props.children}
            </MiddleSection>
        </SwipeCard>
    );
}

export function VideoSection(props) {
    return (
        <SwipeCard>
            <HeaderSection>
                <BasicText content={props.headerText} />
            </HeaderSection>
            <View style={{flex: 5}}><VideoPlayer video={props.video} /></View>
            <FooterSection footerStyle={props.footerStyle} />
        </SwipeCard>
    );
}

export function DenseSection(props) {
    return (
        <SwipeCard>
            <View style={[styles.swipeContentSpacing, styles.swipeContentHeader, props.viewStyle, {padding: 35}]}>
                {props.content}
            </View>
        </SwipeCard>
    );
}

export function MiniSection(props) {
    return (
        <View style={[styles.swipeContentSpacing, styles.swipeContentFooter, props.footerStyle]}>
            <BasicText content={props.text} />
        </View>
    )
}

export default class SwipeComponent extends Component {

    constructor(_){
        super(_)
    }

}
