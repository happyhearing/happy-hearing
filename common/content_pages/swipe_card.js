import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  Alert,
  ScrollView, View,
  Dimensions
} from 'react-native';

import styles from '../styles'
import Swiper from 'react-native-swiper';

export default class SwipeCard extends Component {

  constructor(_){
    super(_)
  }

  render() {
    return (
        <View style={{flex: 1}}>
            {this.props.children}
        </View>
    );
  }
}
