import React, {
  Component
} from 'react';

import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  View, Image
} from 'react-native';

import Video from 'react-native-video';

export default class VideoPlayer extends Component {

    constructor(props, context, ...args) {
         super(props, context, ...args);
         this.state = {
             rate: 1,
             volume: 1,
             muted: false,
             resizeMode: 'contain',
             duration: 0.0,
             currentTime: 0.0,
             paused: true,
         };
     }

     onVideoEnd() {
         this.videoPlayer.seek(0);
         this.setState({key: new Date(), currentTime: 0, paused: true});
     }

      onLoad = (data) => {
        this.setState({ duration: data.duration });
      };

      onProgress = (data) => {
        this.setState({ currentTime: data.currentTime });
      };

     getCurrentTimePercentage() {
        if (this.state.currentTime > 0) {
            return parseFloat(this.state.currentTime) / parseFloat(this.state.duration);
        }
        return 0;
     }

     playOrPauseVideo(paused) {
         this.setState({paused: !paused});
     }

     getCurrentTimePercentage(currentTime, duration) {
         if (currentTime > 0) {
             return parseFloat(currentTime) / parseFloat(duration);
         } else {
             return 0;
         }
     }

     render() {
         let {onClosePressed, video, volume} = this.props;
         let {currentTime, duration, paused} = this.state;

         return (
             <View style={styles.fullScreen} key={this.state.key}>
                     <TouchableOpacity style={styles.videoView} onPress={this.playOrPauseVideo.bind(this, paused)}>
                         <Video ref={videoPlayer => this.videoPlayer = videoPlayer}
                                onEnd={this.onVideoEnd.bind(this)}
                                onLoad={this.onLoad.bind(this)}
                                onProgress={this.onProgress.bind(this)}
                                source={video}
                                paused={paused}
                                resizeMode="contain"
                                style={styles.videoContainer}/>
                         {paused && <Image style={styles.videoIcon} source={require("../../images/play_icon.gif")}/>}
                     </TouchableOpacity>
                     <View style={styles.controls}>
                         <View style={styles.generalControls}>
                             <View style={styles.progress}>
                                <View style={[styles.progressCompleted, { flex: parseFloat(currentTime) / parseFloat(duration) * 100 }]} />
                                <View style={[styles.progressRemaining, { flex: (1 - parseFloat(currentTime) / parseFloat(duration)) * 100 }]} />
                             </View>
                         </View>
                     </View>
             </View>
         );
     }
 }

 let styles = StyleSheet.create({
     fullScreen: {flex: 1, backgroundColor: "black"},
     controller: {
         height: 100,
         justifyContent: "center",
         alignItems: "center"
     },
     controllerButton: {height: 20, width: 20},
     videoView: {
         flex: 1,
         justifyContent: "center",
         alignItems: "center",
     },
     progressBar: {
         alignSelf: "stretch",
         margin: 10
     },
     videoContainer: {
         position: "absolute",
         top: 0,
         left: 0,
         bottom: 0,
         right: 0
     },
     videoIcon: {
         position: "relative",
         alignSelf: "center",
         width: 50,
         height: 50,
         bottom: 0,
         left: 0,
         right: 0,
         top: 0
     },
     generalControls: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 4,
        overflow: 'hidden',
        paddingBottom: 10,
     },
     controls: {
        backgroundColor: 'transparent',
        borderRadius: 0,
        position: 'absolute',
        bottom: 0,
        left: 20,
        right: 20,
     },
     progress: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 5,
        overflow: 'hidden',
     },
     progressCompleted: {
        height: 10,
        backgroundColor: 'blue',
     },
     progressRemaining: {
        height: 10,
        backgroundColor: '#2C2C2C',
     }
 });