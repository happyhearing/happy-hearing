import React, { Component } from 'react';
import {
  TouchableHighlight,
  Text, View, Image
} from 'react-native';

import Button from 'apsl-react-native-button'
import styles from './styles'

export default class MenuButton extends Component {
  render() {
    return (
        <Button
            style={{
                    backgroundColor:'ghostwhite',
                    shadowColor: '#FFF',
                    shadowOffset: {width: 2, height: 2},
                    shadowRadius: 5,
                    shadowOpacity: 0.5,
                    borderRadius: 0,
                    borderWidth: 1,
                    borderColor: '#ccc',
                    padding: 0,
                    marginBottom: 0
            }}
            onPress={this.props.onPress}>
            <Text style={{ color:'#000', fontWeight:'bold' }}>
                {this.props.text}
            </Text>
        </Button>
    );
  }
}
