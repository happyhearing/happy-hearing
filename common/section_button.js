import React, { Component } from 'react';
import { Text, View } from 'react-native';
import Button from 'apsl-react-native-button'

import styles from './styles'

export default class SectionButton extends Component {
  render() {
    return (
        <Button 
            style={{
                    backgroundColor:'ghostwhite',
                    shadowColor: '#000',
                    shadowOffset: {width: 2, height: 2},
                    shadowRadius: 5,
                    shadowOpacity: 0.5,
                    elevation: 5
            }}
            onPress={this.props.onPress}>
            <Text style={{ color:'indigo', fontWeight:'bold' }}>
                {this.props.title}
            </Text>
        </Button>
    );
  }
}

