import {
  StyleSheet,
  Dimensions,
  Platform,
  PixelRatio
} from 'react-native';

const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');

// based on iphone 5s's scale
const scale = SCREEN_WIDTH / 320;

export function normalize(size) {
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(size))
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(size)) - 2
  }
}

const styles = StyleSheet.create({
  wrapper: {
  },
  navButton: {
    padding: 15,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'ghostwhite',
    borderTopWidth: 2,
    borderTopColor: "#ccc"
  },
  navText: {
    alignSelf: 'center',
    flex: 1
  },
  swipeImageContentSpacing: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    paddingLeft: 25,
    paddingRight: 25
  },
  swipeContentSpacing: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    padding: normalize(30)
  },
  swipeContentHeader: {
    flex: 1,
    backgroundColor: '#765097'
  },
  swipeContentImage: {
    flex: 3,
    backgroundColor: '#765097'
  },
  swipeContentVideo: {
      flex: 20,
      backgroundColor: '#765097',
  },
  swipeContentFooter: {
    flex: 1,
    backgroundColor: '#765097'
  },
  swipeContentText: {
    color: '#FFF',
    //fontWeight: 'bold'
  },
  headerText: {
    backgroundColor: '#765097',
    color: '#FFFFFF',
    fontSize: normalize(20),
    textAlign: 'center',
    padding: normalize(10),
  },
  contentText: {
    color: '#000',
    fontSize: normalize(23),
    paddingLeft: 20,
    paddingRight: 20
  },
  extraInfoText: {
    color: '#FFF',
    textAlign: 'left',
    fontSize: normalize(18),
  },
  contentTodo: {
    color: '#ff0000',
    fontSize: normalize(30),
    fontWeight: 'bold',
  },
  image: {
    flex: 1,
    margin: 4,
    borderWidth: 2,
    borderColor: "#000",
    height: undefined,
    width: undefined,
    alignSelf: 'stretch',
    resizeMode: 'stretch'
  },
  logo: {
    height: normalize(60),
    width: normalize(150)
  },
  arrow: {
    justifyContent: 'center',
    flex: 1,
    resizeMode: 'contain',
    height: normalize(50),
    width: normalize(50)
  },
  denseSectionText: {
    fontSize: normalize(16),
    color: "#FFF"
  },
  denseSectionHeader: {
    fontSize: normalize(20),
    fontWeight: 'bold'
  },
  backgroundVideo: {
    width: 350, height: 150,
  },
  textEmphasis: {
    fontWeight: 'bold'
  },
  textEmphasisi: {
    fontWeight: 'bold',
    fontStyle: 'italic'
  }
})

export default styles;
