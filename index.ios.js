import React, { Component } from 'react';
import { AppRegistry } from 'react-native';

import HHNavigator from './HHNavigator';

export default class HappyHearing extends Component {
  render() {
    return(
      <HHNavigator />
    )
  }
}

AppRegistry.registerComponent('HappyHearing', () => HappyHearing);
