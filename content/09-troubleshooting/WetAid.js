import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';

import { DenseSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class InTheEar extends Component {
  static navigationOptions = {
    title: 'Problem: Aid gets wet',
  };
  render() {
    return (
      <DefaultSwipePage title="Problem: Aid gets wet">

        <DenseSection
            content={
                <View>
                    <Text style={styles.denseSectionText}>
                        <Text style={styles.denseSectionHeader}>Problem: Aid gets wet</Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Open holder and allow to drain.
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            A fan may assist drying but never use a hair dryer except on
                            <Text style={{fontWeight: 'bold'}}> cold</Text>,
                            <Text style={{fontWeight: 'bold'}}> slow </Text>setting.
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Leave aid overnight with battery holder open, if possible in an air
                            tight container with drying agent, and check function next morning.
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            If still not working or volume seems low send for repair.
                        </Text>
                    </Text>
                </View>
            }
        />

      </DefaultSwipePage>
    );
  }
}
