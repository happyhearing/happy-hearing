import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';

import { DenseSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class InTheEar extends Component {
  static navigationOptions = {
    title: 'Problem: Continuous whistling',
  };
  render() {
    return (
      <DefaultSwipePage title="Problem: Continuous whistling">

        <DenseSection
            content={
                <View>
                    <Text style={styles.denseSectionText}>
                        <Text style={styles.denseSectionHeader}>Problem: Continuous whistling</Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Is mould fitted snugly in correct ear?
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            If volume can be adjusted, is it too high?
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Is mould too loose?
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Sometimes a light smear of personal lubricant on sides of the mould
                            will stop the whistling (keep holes free of lubricant).
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            An old mould or tubing may have shrunk or cracked. If so, send to
                            hearing services provider for replacement.
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Is ear canal blocked by wax? If so, try clearing with eardrops and
                            if necessary arrange syringing or other procedure by a medical practitioner or audiologist.
                        </Text>
                    </Text>
                </View>
            }
        />

      </DefaultSwipePage>
    );
  }
}
