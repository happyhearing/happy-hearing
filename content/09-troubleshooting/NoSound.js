import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';

import { DenseSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class InTheEar extends Component {
  static navigationOptions = {
    title: 'Problem: No sound',
  };
  render() {
    return (
      <DefaultSwipePage title="Problem: No sound">

        <DenseSection
            content={
                <View>
                    <Text style={styles.denseSectionText}>
                        <Text style={styles.denseSectionHeader}>Problem: No sound</Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Is battery holder fully closed?
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Is battery fitted correctly into battery holder?
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Is battery flat?
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Is mould or tubing clear of wax or moisture?
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Are battery contacts corroded? If so, clean the compartment with an
                            alcohol wipe.
                        </Text>
                    </Text>
                </View>
            }
        />

      </DefaultSwipePage>
    );
  }
}
