import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';

import { DenseSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class InTheEar extends Component {
  static navigationOptions = {
    title: 'If trouble shooting is unsuccessful',
  };
  render() {
    return (
      <DefaultSwipePage title="If trouble shooting is unsuccessful">

        <DenseSection
            content={
                <View>
                    <Text style={styles.denseSectionText}>
                        <Text style={styles.denseSectionHeader}>If trouble shooting is unsuccessful</Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Contact the hearing services provider.
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            If aid needs repair mail to hearing services provider in a rigid
                            container with a note stating problem.
                        </Text>
                    </Text>
                </View>
            }
        />

      </DefaultSwipePage>
    );
  }
}
