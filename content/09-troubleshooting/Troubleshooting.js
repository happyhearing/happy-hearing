import React, { Component } from 'react';
import { Text, ScrollView, View, Button } from 'react-native';
import NoSound from './NoSound';
import ContWhistling from './ContWhistling';
import WeakSound from './WeakSound';
import WetAid from './WetAid';
import Other from './Other';

import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import MenuButton from '../../common/menu_button';
import styles from '../../common/styles';

class Types extends Component {
  static navigationOptions = {
    title: 'Basic trouble shooting',
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <ScrollView>
        <MenuButton text="No sound" onPress={() => navigate('NoSound')} />
        <MenuButton text="Continuous whistling" onPress={() => navigate('ContWhistling')} />
        <MenuButton text="Sound weak" onPress={() => navigate('WeakSound')} />
        <MenuButton text="Aid gets wet" onPress={() => navigate('WetAid')} />
        <MenuButton text="If trouble shooting is unsuccessful" onPress={() => navigate('Other')} />
      </ScrollView>
    );
  }
}

export let Children = {
  NoSound: { screen: NoSound },
  ContWhistling: { screen: ContWhistling },
  WeakSound: { screen: WeakSound },
  WetAid: { screen: WetAid },
  Other: { screen: Other },
};
export default Types;
