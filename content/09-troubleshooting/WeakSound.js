import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';

import { DenseSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class InTheEar extends Component {
  static navigationOptions = {
    title: 'Problem: Sound weak',
  };
  render() {
    return (
      <DefaultSwipePage title="Problem: Sound weak">

        <DenseSection
            content={
                <View>
                    <Text style={styles.denseSectionText}>
                        <Text style={styles.denseSectionHeader}>Problem: Sound weak</Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Does battery need changing?
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            If volume can be adjusted, is it too low?
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Is mould and/or tubing partly blocked by wax or moisture?
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Is tubing twisted, pinched or shrunk with age? If necessary, send to
                            hearing services provider for replacement.
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Is there too much wax in the ear?
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            If none of the above, client’s hearing may have deteriorated and need
                            reassessment.
                        </Text>
                    </Text>
                </View>
            }
        />

      </DefaultSwipePage>
    );
  }
}
