import React, { Component } from 'react';
import { Text, View } from 'react-native';

import { Section, SimpleSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class TurningOn extends Component {
  static navigationOptions = {
    title: 'Directional microphones',
  };
  render() {
    return (
      <DefaultSwipePage title="Directional microphones">

        <SimpleSection
            text="Some aids have directional microphones that may help to reduce background noise."
        />

        <SimpleSection
            text="Changing microphones between omni-directional (surround sound) and directional may occur automatically or manually."
        />

      </DefaultSwipePage>
    );
  }
}
