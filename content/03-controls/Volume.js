import React, { Component } from 'react';
import { Text, ScrollView, Button } from 'react-native';
import { MiniSection } from '../../common/content_pages/swipe_component'
import VolumeBTE from './VolumeBTE';
import VolumeITE from './VolumeITE';
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import MenuButton from '../../common/menu_button';
import styles from '../../common/styles';

class OnOff extends Component {
  static navigationOptions = {
    title: 'Volume',
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <ScrollView>
         <MiniSection
            text="Some aids have a wheel, rocker or lever with which to change the volume of sound. Some aids will have a remote control to make adjustments."
         />
        <MenuButton text="Changing the volume on a Behind The Ear aid" onPress={() => navigate('VolumeBTE')} />
        <MenuButton text="Changing the volume on a In The Ear aid" onPress={() => navigate('VolumeITE')} />
      </ScrollView>
    );
  }
}

export let Children = {
  VolumeBTE: { screen: VolumeBTE },
  VolumeITE: { screen: VolumeITE }
};
export default OnOff;
