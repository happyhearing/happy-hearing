import React, { Component } from 'react';
import { Text, View } from 'react-native';

import { Section, SimpleSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import Button from '../../common/section_button'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class TurningOff extends Component {
  static navigationOptions = {
    title: 'Turning an aid ON',
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <DefaultSwipePage title="Turning an aid ON">

        <SimpleSection text="1. Check there is a battery in the battery holder." />

        <SimpleSection text="">
            <MultiLineText
                firstLine={
                    <Text>
                        <BasicText content="2. Close the battery holder to turn the aid " />
                        <BasicText textStyle={{fontWeight: 'bold'}} content="ON." />
                    </Text>
                }
                secondLine={
                    <BasicText
                        textStyle={{fontStyle: 'italic', fontSize: 18}}
                        content="(Some aids have an on and off switch.)"
                    />
                }
            />
        </SimpleSection>

         <SimpleSection>
                <View style={{flex: 1}}>
                    <View style={{flex: 2}} />
                    <View style={{flex: 2}}>
                        <MultiLineText
                            firstLine="3. Cup hand around aid. It should whistle."
                            secondLine={
                                <BasicText
                                    textStyle={{fontStyle: 'italic', fontSize: 18}}
                                    content="(If necessary, hold aid close to your ear to check for a quiet whistle.)"
                                />
                            }
                        />
                    </View>
                    <View style={{flex: 2}}>
                        <Button title="Aid ON but no whistle?" onPress={() => navigate('Troubleshooting')} />
                    </View>
                </View>
        </SimpleSection>

        <SimpleSection>
            <View style={{flex: 1}}>
                <View style={{flex: 1}}>
                    <BasicText content="4. Aids will come on at a generally suitable program and volume." />
                </View>
                <View style={{flex: 1}} />
                <View style={{flex: 2}}>
                    <BasicText
                        textStyle={{fontSize: 18}}
                        content="Programs for different situations are sometimes available on aids for different listening environments e.g. noisy areas."
                    />
                </View>
                <View tyle={{flex: 2}}>
                    <BasicText
                        textStyle={{fontSize: 18}}
                        content="These may then vary automatically depending on the surrounding situation (e.g. in background noise) and/or be adjusted manually."
                    />
                </View>
                <View style={{flex: 1}} />
                <View style={{flex: 1}}>
                    <Button title="Other programs" onPress={() => navigate('MultiPrograms')} />
                </View>
            </View>
        </SimpleSection>
      </DefaultSwipePage>
    );
  }
}
