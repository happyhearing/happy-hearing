import React, { Component } from 'react';
import { Text, View } from 'react-native';

import { Section, SimpleSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class TurningOn extends Component {
  static navigationOptions = {
    title: 'Telecoil / T-switch',
  };
  render() {
    return (
      <DefaultSwipePage title="Telecoil / T-switch">

        <SimpleSection
            text="Many aids have a Telecoil for use with telephones or in ‘looped’ areas. Telecoils can improve clarity of sound received by a hearing aid and reduce surrounding background noise."
        />

        <SimpleSection
            text="Looped environments may include community rooms in aged care facilities and are sometimes also in cinemas, churches, booking offices, banks etc."
        />

        <Section
            headerText="The symbol below indicates that a loop is available in the location."
            imageSource={require('./03-controls-021.jpg')}
            imageStyle={{marginLeft: 40, marginRight: 40}}
        />

        <SimpleSection
            text="The Telecoil is activated by switching to the ‘T’ setting and back to the ‘M’ setting when Telecoil usage is completed."
        />

        <SimpleSection
            text="Not all aids have a Telecoil function. Some aids have switchless Telecoils that are automatically activated when a phone is held near the aid."
        />

        <SimpleSection
            text="If there is a program button or switch, then a Telecoil program may be able to be set up to be accessed manually."
        />

      </DefaultSwipePage>
    );
  }
}
