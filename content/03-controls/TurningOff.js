import React, { Component } from 'react';
import { Text, View } from 'react-native';

import { Section, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class TurningOn extends Component {
  static navigationOptions = {
    title: 'Turning an aid OFF',
  };
  render() {
    return (
      <DefaultSwipePage title="Turning an aid OFF">

        <Section
            middleText={
                <MultiLineText
                    firstLine={
                        <Text>
                            <BasicText content="Open the battery holder to turn the aid " />
                            <BasicText textStyle={{fontWeight: 'bold'}} content="OFF." />
                        </Text>
                    }
                    secondLine={
                        <BasicText
                            textStyle={{fontStyle: 'italic', fontSize: 18}}
                            content="(Some aids have an on and off switch.)"
                        />
                    }
                />
            }
        />

      </DefaultSwipePage>
    );
  }
}
