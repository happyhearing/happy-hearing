import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';

import { Section, DenseSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class TurningOn extends Component {
  static navigationOptions = {
    title: 'Changing the volume on a BTE aid',
  };
  render() {
    return (
      <DefaultSwipePage title="Volume BTE">

        <Section
            headerText="On a BTE aid move the volume control up towards the ceiling to increase the volume or tap the upper part of the rocker or lever."
            imageSource={require('./03-controls-019.jpg')}
        />

        <Section
            headerText="Move the volume control down to decrease the volume or tap the lower part of the rocker or lever."
            imageSource={require('./03-controls-020.jpg')}
        />



        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <BasicText content="If a pair of BTEs have only one control on each aid, the aids are probably interconnected electronically." />
                    </View>
                    <View style={{flex: 1}}>
                        <BasicText content="One end of the rocker control on one aid adjusts the programs in both aids and the other end of the rocker on the other aid adjusts the volume of both aids." />
                    </View>
                </View>
            }
        />

      </DefaultSwipePage>
    );
  }
}
