import React, { Component } from 'react';
import { Text, View } from 'react-native';

import { Section, SimpleSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class TurningOn extends Component {
  static navigationOptions = {
    title: 'Multi programs',
  };
  render() {
    return (
      <DefaultSwipePage title="Multi programs">

        <SimpleSection
            text={
                <Text>
                    <Text>
                        Programs for different situations are sometimes set up on aids for different listening environments.
                    </Text>
                    <Text>{"\n\n"}</Text>
                    <Text>
                        For example, programs may be set for quiet areas, noisy areas or listening to music.
                    </Text>
                </Text>
            }
        />

        <SimpleSection
            text="When initially turned on the aid defaults to Program 1 which is set by an audiologist and is usually the most appropriate for the user’s needs."
        />


        <SimpleSection
            text={
                <Text>
                    <Text>
                        To change the program, users press the button on the aid.
                    </Text>
                    <Text>{"\n\n"}</Text>
                    <Text>
                        They may be alerted to the program they have selected by a number beeps that correlate to the program they are in.
                    </Text>
                    <Text>{"\n\n"}</Text>
                    <Text>
                        E.g. If they have selected Program 3 then they will hear 3 beeps.
                    </Text>
                </Text>
            }
        />

        <SimpleSection
            text="Sometimes programs may be accessed by a remote control."
        />

      </DefaultSwipePage>
    );
  }
}
