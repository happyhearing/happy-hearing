import React, { Component } from 'react';
import { Text, View, ScrollView, Button } from 'react-native';
import OnOff, { Children as OnOffChildren } from './OnOff';
import Volume, { Children as VolumeChildren } from './Volume';
import OtherPrograms, { Children as OtherProgramsChildren } from './OtherPrograms';
import Telecoils from './Telecoils';
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import MenuButton from '../../common/menu_button';
import styles from '../../common/styles';

class Controls extends Component {
  static navigationOptions = {
    title: 'Hearing aid controls',
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
        <ScrollView>
          <MenuButton text="Turning hearing aids ON and OFF" onPress={() => navigate('OnOff')} />
          <MenuButton text="Volume" onPress={() => navigate('Volume')} />
          <MenuButton text="Telecoils" onPress={() => navigate('Telecoils')} />
          <MenuButton text="Other programs" onPress={() => navigate('OtherPrograms')} />
        </ScrollView>
    );
  }
}

export let Children = {
  OnOff: { screen: OnOff },
  Volume: { screen: Volume },
  Telecoils: { screen: Telecoils },
  OtherPrograms: { screen: OtherPrograms },
  ...OnOffChildren,
  ...VolumeChildren,
  ...OtherProgramsChildren
};
export default Controls;
