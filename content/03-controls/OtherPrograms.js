import React, { Component } from 'react';
import { Text, ScrollView, Button } from 'react-native';
import MultiPrograms from './MultiPrograms';
import DirectionalMics from './DirectionalMics';
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import MenuButton from '../../common/menu_button';
import styles from '../../common/styles';

class OnOff extends Component {
  static navigationOptions = {
    title: 'Other programs',
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <ScrollView>
        <MenuButton text="Multi programs" onPress={() => navigate('MultiPrograms')} />
        <MenuButton text="Directional microphones" onPress={() => navigate('DirectionalMics')} />
      </ScrollView>
    );
  }
}

export let Children = {
  MultiPrograms: { screen: MultiPrograms },
  DirectionalMics: { screen: DirectionalMics }
};
export default OnOff;
