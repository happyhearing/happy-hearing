import React, { Component } from 'react';
import { Text, View } from 'react-native';

import { Section, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class TurningOn extends Component {
  static navigationOptions = {
    title: 'Changing the volume on a ITE aid',
  };
  render() {
    return (
      <DefaultSwipePage title="Volume ITE">

        <Section
            headerText="On an ITE aid move the volume control towards the face to increase the volume."
            imageSource={require('./03-controls-017.jpg')}
        />

        <Section
            headerText="Move the volume control towards the back of the head to decrease the volume."
            imageSource={require('./03-controls-018.jpg')}
        />

      </DefaultSwipePage>
    );
  }
}
