import React, { Component } from 'react';
import { Text, ScrollView, Button } from 'react-native';
import TurningOff from './TurningOff';
import TurningOn from './TurningOn';
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import MenuButton from '../../common/menu_button';
import styles from '../../common/styles';

class OnOff extends Component {
  static navigationOptions = {
    title: 'Turning aids ON and OFF',
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <ScrollView>
        <MenuButton text="Turning an aid ON" onPress={() => navigate('TurningOn')} />
        <MenuButton text="Turning an aid OFF" onPress={() => navigate('TurningOff')} />
      </ScrollView>
    );
  }
}

export let Children = {
  TurningOn: { screen: TurningOn },
  TurningOff: { screen: TurningOff }
};
export default OnOff;
