import React, { Component } from 'react';
import { Text, ScrollView, View, Button } from 'react-native';
import Available from './Available';
import NotAvailable from './NotAvailable';
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import MenuButton from '../../common/menu_button';
import styles from '../../common/styles';

class Types extends Component {
  static navigationOptions = {
    title: 'Attaching new BTE mould to an aid',
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <ScrollView>
        <View style={{padding: 10, flex: 1, justifyContent: 'center'}}>
            <Text style={{textAlign: 'center', color: "#000"}}>
                Sometimes a new BTE mould may be mailed by a hearing services provider to a client
                so as not to delay fitting.
            </Text>
        </View>
        <MenuButton text="Old mould is still available" onPress={() => navigate('Available')} />
        <MenuButton text="Old mould is not available" onPress={() => navigate('NotAvailable')} />
      </ScrollView>
    );
  }
}

export let Children = {
  Available: { screen: Available },
  NotAvailable: { screen: NotAvailable }
};
export default Types;
