import React, { Component } from 'react';
import { Text, View, ScrollView, Image } from 'react-native';

import { DenseSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class BehindTheEar extends Component {
  static navigationOptions = {
    title: 'If the old mould is NOT available',
  };

  render() {
    return (
      <DefaultSwipePage title="If the old mould is NOT available">


        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text style={styles.denseSectionHeader}>Step one</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                Insert new mould with tubing attached into ear so it is completely tucked into place.
                            </Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                    <View style={{flex: 2, alignItems: 'center', marginBottom: 30}}>
                        <Image style={styles.image} style={styles.image} source={require('./08-attaching-043.jpg')} />
                    </View>
                </View>
            }
        />

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <Text style={styles.denseSectionText}>
                        <Text style={styles.denseSectionHeader}>Step two</Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Place hearing aid without any tubing or mould behind the ear in a
                            comfortable position.
                        </Text>
                        <Text>{"\n\n"}</Text>
                    </Text>
                </View>
            }
        />

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text style={styles.denseSectionHeader}>Step three</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                On the tubing mark with a pen the place 5mm above where the earhook ends.
                            </Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                    <View style={{flex: 2, alignItems: 'center', marginBottom: 30}}>
                        <Image style={styles.image} style={styles.image} source={require('./08-attaching-041.jpg')} />
                    </View>
                </View>
            }
        />

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text style={styles.denseSectionHeader}>Step four</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                Remove the mould
                                and cut the tubing on the pen mark.
                            </Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                    <View style={{flex: 2, alignItems: 'center', marginBottom: 30}}>
                        <Image style={styles.image} style={styles.image} source={require('./08-attaching-042.jpg')} />
                    </View>
                </View>
            }
        />

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text style={styles.denseSectionHeader}>Step five</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                Push tubing of new mould over end of earhook, making sure the mould is
                                in correct direction to fit the ear.
                            </Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                    <View style={{flex: 2, alignItems: 'center', marginBottom: 30}}>
                        <Image style={styles.image} style={styles.image} source={require('./08-attaching-040.jpg')} />
                    </View>
                </View>
            }
        />

      </DefaultSwipePage>
    );
  }
}
