import React, { Component } from 'react';
import { Text, View, ScrollView, Image } from 'react-native';

import { DenseSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class BehindTheEar extends Component {
  static navigationOptions = {
    title: 'If the old mould is available',
  };

  render() {
    return (
      <DefaultSwipePage title="If the old mould is available">

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text style={styles.denseSectionHeader}>Step one</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                Remove old mould from the hearing aid by pulling the tubing away from the earhook.
                            </Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                    <View style={{flex: 2, alignItems: 'center', marginBottom: 20}}>
                        <Image style={styles.image} style={styles.image} source={require('./08-attaching-039.jpg')} />
                    </View>
                </View>
            }
        />

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text style={styles.denseSectionHeader}>Step two</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                Measure tubing of new mould against tubing of old mould.
                            </Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                    <View style={{flex: 2, alignItems: 'center', marginBottom: 20}}>
                        <Image style={styles.image} style={styles.image} source={require('./08-attaching-044.jpg')} />
                    </View>
                </View>
            }
        />

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text style={styles.denseSectionHeader}>Step three</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                Cut tubing on new mould to same length as on old mould.
                            </Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                    <View style={{flex: 2, alignItems: 'center', marginBottom: 20}}>
                        <Image style={styles.image} style={styles.image} source={require('./08-attaching-045.jpg')} />
                    </View>
                </View>
            }
        />

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text style={styles.denseSectionHeader}>Step four</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                Push tubing of new mould over end of earhook, making sure the mould is
                                in correct direction to fit the ear.
                            </Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                    <View style={{flex: 2, alignItems: 'center', marginBottom: 20}}>
                        <Image style={styles.image} style={styles.image} source={require('./08-attaching-046.jpg')} />
                    </View>
                </View>
            }
        />

      </DefaultSwipePage>
    );
  }
}
