import React, { Component } from 'react';
import { Text, View, ScrollView, Image, Linking } from 'react-native';

import { DenseSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class FurtherInfo extends Component {
  static navigationOptions = {
    title: 'Disclaimer',
  };

  render() {
    return (
      <DefaultSwipePage title="Disclaimer">

        <DenseSection
            content={
                <View>
                    <Text style={[styles.denseSectionText, {fontSize: 18}]}>
                        <Text>
                            While every effort has been made to ensure that material on this app is accurate and up to date at time of publication such material does not constitute the
                            provision of professional advice (this is particularly so as hearing devices can vary considerably). Deafness Forum of Australia does not guarantee, and
                            accepts no legal liability whatsoever arising from or connected to, the accuracy, reliability, currency or completeness of any material contained on this app
                            or any linked site.
                        </Text>
                    </Text>
                </View>
            }
        />

      </DefaultSwipePage>
    );
  }
}
