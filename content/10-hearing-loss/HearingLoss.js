import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';

import { Section, SimpleSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class InTheEar extends Component {
  static navigationOptions = {
    title: 'Common signs of hearing loss',
  };
  render() {
    return (

      <DefaultSwipePage title="Common signs of hearing loss">

        <SimpleSection
            text={
                <Text style={{fontWeight: 'bold'}}>Any one of the following behaviours could indicate the need for a hearing test:</Text>
            }
        />

        <SimpleSection
            text="Repetition frequently requested."
        />

        <SimpleSection
            text="Loud volume of TV or radio."
        />

        <SimpleSection
            text="Difficulty understanding conversation in groups or noise."
        />

        <SimpleSection
            text="Watches speaker’s face and gestures intently."
        />

        <SimpleSection
            text="Misses what is said, especially if speaker is not facing them."
        />

        <SimpleSection
            text="Difficulty hearing phone ‘rings’ and/or hearing a speaker on the phone."
        />

        <SimpleSection
            text="Responds only to loud speech or sounds and responses are sometimes inconsistent with conversation."
        />

        <SimpleSection
            text="Withdrawing from social activities."
        />

      </DefaultSwipePage>
    );
  }
}
