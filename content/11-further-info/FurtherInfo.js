import React, { Component } from 'react';
import { Text, View, ScrollView, Image, Linking } from 'react-native';

import { DenseSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class FurtherInfo extends Component {
  static navigationOptions = {
    title: 'Further information',
  };

  render() {
    return (
      <DefaultSwipePage title="Further information">

        <DenseSection
            content={
                <View>
                    <Text style={[styles.denseSectionText, {fontSize: 15}]}>
                        <Text>For additional information view the free online Deafness Forum video </Text>
                        <Text style={{fontStyle: 'italic'}}>Hearing Assistance in Aged Care </Text>
                        <Text style={[styles.textEmphasise, {color: 'blue'}]}
                              onPress={() => Linking.openURL('https://www.youtube.com/watch?v=O15xOkOkFVQ&feature=')}>
                          here
                        </Text>
                        <Text>.{"\n"}This video is suitable for family carers and volunteers as well as for paid staff.{"\n\n"}</Text>

                        <Text>A free online HEARnet Learning module </Text>
                        <Text style={{fontStyle: 'italic'}}>Hearing Assistance for Elderly Clients </Text>
                        <Text>incorporates the above Deafness Forum video and includes multiple choice quizzes and certification of satisfactory completion. </Text>
                        <Text>{"\n\n"}The HEARNET Learning module is ideal for individual in-service or CPD purposes. The module is accredited through the Australian Academy of Audiology.</Text>
                        <Text>{"\n"}Enrolment is simple and quick. Find it </Text>
                        <Text style={[styles.textEmphasise, {color: 'blue'}]}
                              onPress={() => Linking.openURL('https://hearnetlearning.org.au/enrol/index.php?id=56')}>
                          here
                        </Text>
                        <Text>.</Text>
                    </Text>
                </View>
            }
        />

      </DefaultSwipePage>
    );
  }
}
