import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';

import { Section, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class InTheEar extends Component {
  static navigationOptions = {
    title: 'In the Ear aids',
  };
  render() {
    return (
      <DefaultSwipePage title="In-The-Ear (ITE) aids">

        <Section
            headerText="Hearing aid worn in the ear."
            imageSource={require('./02-types-005.jpg')}
        />

        <Section
            headerText="Hearing aid worn in the ear."
            imageSource={require('./02-types-006.jpeg')}
            imageStyle={{
                resizeMode: 'contain'
            }}
        />

      </DefaultSwipePage>
    );
  }
}
