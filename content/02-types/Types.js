import React, { Component } from 'react';
import { Text, ScrollView, View, Button } from 'react-native';
import InTheEar from './InTheEar';
import BehindTheEar from './BehindTheEar';
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import MenuButton from '../../common/menu_button';
import styles from '../../common/styles';

class Types extends Component {
  static navigationOptions = {
    title: 'Types and parts of hearing aids',
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <ScrollView>
        <MenuButton text="Behind the ear (BTE) aids" onPress={() => navigate('BehindTheEar')} />
        <MenuButton text="In the ear (ITE) aids" onPress={() => navigate('InTheEar')} />
        <View style={{padding: 10, marginTop: 20, flex: 1, justifyContent: 'center'}}>
            <Text style={{textAlign: 'center', color: "darkgray"}}>
                Consult manufacturer manual if available for specific aids.
            </Text>
        </View>
      </ScrollView>
    );
  }
}

export let Children = {
  InTheEar: { screen: InTheEar },
  BehindTheEar: { screen: BehindTheEar }
};
export default Types;
