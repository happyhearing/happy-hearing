import React, { Component } from 'react';
import { Text, View, ScrollView, Image } from 'react-native';

import { Section, MultiLineText, BasicText, SimpleSection } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class BehindTheEar extends Component {
  static navigationOptions = {
    title: 'Behind the Ear aids',
  };

  render() {
    return (
      <DefaultSwipePage title="Behind-The-Ear (BTE) aids">

        <SimpleSection text="Aid sits behind the ear and is connected to an ear mould or device by tubing or wire." />

        <Section
            headerText="Example of a BTE aid with full ear mould and alternative volume and program controls."
            imageSource={require('./02-types-001.jpg')}
        />

        <Section
            headerText="Example of a smaller style BTE with full ear mould."
            imageSource={require('./02-types-002.jpg')}
        />

        <Section
            headerText="An example of a smaller style BTE aid with ‘open fit’ device."
            imageSource={require('./02-types-003.jpg')}
            footerContent={
                <View>
                    <Text style={styles.extraInfoText}>
                        Open-fit aids do not require an ear mould.
                    </Text>
                    <Text />
                    <Text style={styles.extraInfoText}>
                        Domes and tubing should be replaced reasonably frequently.
                        Hearing services provider that supplied the aids can provide right sized
                        domes and tubing for individual users.
                    </Text>
                </View>
            }
            footerStyle={{flex: 3, paddingTop: -50}}
        />

        <Section
            headerText="Example of a smaller style BTE aid with receiver in canal."
            imageSource={require('./02-types-004.jpg')}
            footerContent={
                <View style={styles.contentSlide}>
                    <Text style={styles.extraInfoText}>
                        Similar to ‘open fit’ hearing aids, but they have the receiver in the ear canal instead of inside the hearing aid.
                    </Text>
                </View>
            }
            footerStyle={{flex: 1, marginTop: 0}}
        />

      </DefaultSwipePage>
    );
  }
}
