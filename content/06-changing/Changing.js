import React, { Component } from 'react';
import { Text, Image, View } from 'react-native';

import { Section, DenseSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import Button from '../../common/section_button'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class TurningOn extends Component {
  static navigationOptions = {
    title: 'Changing an aid battery',
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <DefaultSwipePage title='Changing an aid battery'>

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <Text style={styles.denseSectionText}>
                        <Text style={styles.denseSectionHeader}>Step one</Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Open battery holder carefully with thumb nail.
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Remove battery and discard immediately.
                        </Text>
                        <Text>{"\n\n"}</Text>
                    </Text>
                </View>
            }
        />

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <Text style={styles.denseSectionText}>
                        <Text style={styles.denseSectionHeader}>Step two</Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Check new battery’s size and use by date.
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Peel vinyl tab off the battery and insert into holder with flat side
                            (marked +) facing upwards then gently close battery door fully.
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            (Preferably use magnetic tip of wax brush to lift battery in or out.)
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Valuable tip:{"\n"}
                            If possible, to prolong battery life, wait one to two minutes between
                            peeling off vinyl tab and closing the battery holder.
                        </Text>
                    </Text>
                </View>
            }
        />

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text>Battery compartment of a BTE aid</Text>
                        </Text>
                    </View>
                    <View style={{flex: 3, alignItems: 'center', marginBottom: 20}}>
                        <Image style={styles.image} source={require('./06-changing-028.jpg')} />
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text>Battery compartment of an ITE aid</Text>
                        </Text>
                    </View>
                    <View style={{flex: 3, alignItems: 'center', marginBottom: 20}}>
                        <Image style={styles.image} source={require('./06-changing-029.jpg')} />
                    </View>
                </View>
            }
        />

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <Text style={styles.denseSectionText}>
                        <Text style={styles.denseSectionHeader}>
                            Step three
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Cup aid in hand to check for whistle.
                        </Text>
                        <Text>{"\n\n"}</Text>
                    </Text>
                    <Button title="Inserting aid into the ear" onPress={() => navigate('Inserting')} />
                    <Text style={styles.denseSectionText}>
                        <Text>{"\n\n"}</Text>
                        <Text style={styles.denseSectionHeader}>
                            Strong recommendation:
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            If the hearing aid user forgets
                            or does not recognise an aid’s warning sound that the battery needs
                            changing, then it is best to routinely change batteries on the same day
                            each week (or more frequently if necessary)
                            <Text> and </Text>
                            <Text style={{textDecorationLine: 'underline'}}>order replacement batteries when needed, normally when only one packet of batteries remains.</Text>
                        </Text>

                    </Text>
                </View>
            }
        />

      </DefaultSwipePage>
    );
  }
}
