import React, { Component } from 'react';
import { Text, Image, View } from 'react-native';
import Video from 'react-native-video';

import { Section, VideoSection, DenseSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import Button from '../../common/section_button'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class TurningOn extends Component {
  static navigationOptions = {
    title: 'Inserting aid into the ear',
  };

  video: Video;

  state = {
    rate: 1,
    volume: 1,
    muted: false,
    resizeMode: 'contain',
    duration: 0.0,
    currentTime: 0.0,
    paused: true,
  };

  onProgress = (data) => {
    this.setState({ currentTime: data.currentTime });
  };

  onEnd = () => {
    this.setState({ paused: true })
    this.video.seek(0)
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <DefaultSwipePage title="Inserting aid into the ear">

        <VideoSection
            headerText="Watch: Inserting a BTE aid in an ear"
            video={require('./insertingBTE.mp4')}
        />

        <VideoSection
            headerText="Watch: Inserting an ITE aid in an ear"
            video={require('./insertingITE.mp4')}
        />

        <DenseSection
            content={
                <Text style={styles.denseSectionText}>
                    <Text style={{fontSize: 20, fontWeight: 'bold', color: 'red'}}>Cautions!</Text>
                    <Text>{"\n\n"}</Text>
                    <Text>Hearing aids are expensive and easily damaged - handle them with clean, dry hands over a soft surface in case an aid is dropped - and never over water.</Text>
                    <Text>{"\n\n"}</Text>
                    <Text>
                        <Text>Aids </Text>
                        <Text style={{fontWeight: 'bold'}}>should not </Text>
                        <Text>be: </Text>
                        <Text>{"\n"}</Text>
                        <Text>* worn in the bath or shower or</Text>
                        <Text>{"\n"}</Text>
                        <Text>* under a hair dryer.</Text>
                    </Text>
                    <Text>{"\n\n"}</Text>
                    <Text>Do not use hairspray after inserting a hearing aid.</Text>
                    <Text>{"\n\n"}</Text>
                    <Text>
                        <Text>Aids </Text>
                        <Text style={{fontWeight: 'bold'}}>should </Text>
                        <Text>be: </Text>
                        <Text>{"\n"}</Text>
                        <Text>* taken out when going to sleep.</Text>
                    </Text>
                </Text>
            }
        />

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text style={styles.denseSectionHeader}>Step one</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>Wash hands with water or antibacterial gel.</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                Wipe aid with tissue and brush away any wax from mould.
                                Use wearer’s own brush (to avoid possible cross infection).
                            </Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                    <View style={{flex: 1, alignItems: 'center'}}>
                        <Image style={[styles.image, {marginLeft: 40, marginRight: 40}]} source={require('./04-inserting-022.jpg')} />
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text>{"\n"}</Text>
                            <Text>Use cleaning brush to gently remove wax from sound or air holes in a BTE aid’s mould, or an ITE aid.</Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                        <Button title="Types and parts of hearing aids" onPress={() => navigate('Types')} />
                        <Text>{"\n"}</Text>
                    </View>
                </View>
            }
        />

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 1, marginBottom: 20}}>
                        <Text style={styles.denseSectionText}>
                            <Text style={styles.denseSectionHeader}>Step two</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>If wax or dirt remains lodged in the sound or air holes of a BTE mould or the air hole of an ITE aid after brushing, use a cleaning wire to remove.</Text>
                            <Text>{"\n\n"}</Text>
                            <Text><Text style={{fontWeight: 'bold', fontStyle: 'italic', textDecorationLine: 'underline'}}>Never</Text> push a cleaning wire into the sound hole on an ITE aid as this can damage the receiver.</Text>
                        </Text>
                    </View>
                    <View style={{flex: 1, alignItems: 'center', marginLeft: 80, marginRight: 80}}>
                        <Image style={styles.image} source={require('./04-inserting-023.jpg')} />
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text>{"\n"}</Text>
                            <Text>Sometimes a special wax removal system is provided for ITE aids. See manufacturer’s manual.</Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                        <Button title="Types and parts of hearing aids" onPress={() => navigate('Types')} />
                        <Text>{"\n"}</Text>
                    </View>
                </View>
            }
        />

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <Text style={styles.denseSectionText}>
                        <Text style={styles.denseSectionHeader}>Step three</Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                        If wax blockage in a BTE aid mould or tubing cannot be removed by
                        brushing, or with a cleaning wire try removing blockage with an air blower.
                        (Air blowers can be purchased from a hearing services provider.)
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>If unsuccessful wash the mould and tubing.</Text>
                        <Text>{"\n\n"}</Text>
                    </Text>
                    <Button title="Washing BTE aid moulds and tubing" onPress={() => navigate('Washing')} />
                    <Text>{"\n"}</Text>
                </View>
            }
        />

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text style={styles.denseSectionHeader}>Step four</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>Close battery holder carefully to turn aid on then cup hand around aid. It should whistle.</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>If necessary, hold aid close to your ear to check for a quiet whistle.</Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                    <View style={{flex: 1, alignItems: 'center'}}>
                        <Image style={[styles.image, {marginLeft: 40, marginRight: 40}]} source={require('./04-inserting-024.jpg')} />
                    </View>
                    <View style={{flex: 1}}>
                        <Text>{"\n\n"}</Text>
                        <Button title="Troubleshooting: No sound" onPress={() => navigate('NoSound')} />
                        <Text>{"\n"}</Text>
                    </View>
                </View>
            }
        />

        <DenseSection
            content={
                <View>
                    <Text style={styles.denseSectionText}>
                        <Text style={styles.denseSectionHeader}>Step five</Text>
                        <Text>{"\n\n"}</Text>
                        <Text>Check for any signs of blood, discharge or broken skin.</Text>
                        <Text>{"\n\n"}</Text>
                        <Text>Insert mould carefully and snugly into correct ear.</Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            <Text>Coloured markers indicate which ear the aid should be inserted in: </Text>
                            <Text>{"\n"}</Text>
                            <Text style={[styles.textEmphasis, {color: 'red'}]}>Red - right ear</Text>
                            <Text>{"\n"}</Text>
                            <Text style={[styles.textEmphasis, {color: 'blue'}]}>Blue - left ear</Text>
                            <Text>{"\n"}</Text>
                        </Text>
                        <Text>Markers are typically found on tip of mould or label on aid.</Text>
                        <Text>{"\n\n"}</Text>
                        <Text>With BTE aid ensure that tubing is not twisted or pinched and that aid sits comfortably behind ear.</Text>
                        <Text>{"\n\n"}</Text>
                        <Text>Sometimes water based lubricant is required to correctly position the mould and/or avoid pain.</Text>
                        <Text>{"\n\n"}</Text>
                        <Text>Make sure lubricant does not block sound hole or air vent by lightly smearing lubricant on sides of mould only.</Text>
                    </Text>
                </View>
            }
        />

        <DenseSection
            content={
                <View>
                    <Text style={styles.denseSectionText}>
                        <Text style={styles.denseSectionHeader}>Step six</Text>
                        <Text>{"\n\n"}</Text>
                        <Text>Make sure the aid is not whistling after inserted into the ear.</Text>
                        <Text>{"\n\n"}</Text>
                    </Text>
                    <Button title="Troubleshooting: Continuous whistling" onPress={() => navigate('ContWhistling')} />
                    <Text style={styles.denseSectionText}>
                        <Text>{"\n"}</Text>
                        <Text>Step back, turn aside and ask a straightforward question to check the aid is functioning satisfactorily.</Text>
                    </Text>
                </View>
            }
        />

      </DefaultSwipePage>
    );
  }
}
