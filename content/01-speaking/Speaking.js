import React, { Component } from 'react';
import { Text } from 'react-native';

import { Section, SimpleSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import Button from '../../common/section_button'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'

import styles from '../../common/styles';

export default class Speaking extends Component {
  static navigationOptions = {
    title: "Speaking to a hearing impaired person"
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <DefaultSwipePage title="Speaking to a hearing impaired person">

        <Section
            headerText={
                <Text>
                    <BasicText content="First, get the person's " />
                    <Text style={styles.textEmphasis}>attention </Text>
                    <BasicText content="in a way that doesn't annoy or embarrass them. Never speak from another room." />
                </Text>
            }
            imageSource={require('./01-speaking-001-1.jpg')}
            imageStyle={{
                resizeMode: 'contain'
            }}
            footerContent={
                <Button onPress={() => navigate('HearingLoss')} title="Signs of hearing loss" />
            }
        />

        <Section
            headerText={
                <Text>
                    <BasicText content="Reduce or move away from " />
                    <Text style={styles.textEmphasis}>background noise </Text>
                    <BasicText content="- turn off the TV, etc." />
                </Text>
            }
            imageStyle={{
                resizeMode: 'contain'
            }}
            imageSource={require('./01-speaking-002.jpg')}
        />

        <Section
            headerText={
                <Text>
                    <Text style={styles.textEmphasisi}>Don't shout! </Text>
                    <BasicText content="Speak normally, if necessary a little louder." />
                </Text>
            }
            imageStyle={{
                marginLeft: 40,
                marginRight: 40

            }}
            imageSource={require('./01-speaking-003.jpg')}
        />

        <Section
            headerText={
                <Text>
                    <Text style={styles.textEmphasis}>Face listener </Text>
                    <BasicText content="at same eye level about one metre apart. Both sit or both stand." />
                </Text>
            }
            imageStyle={{
                resizeMode: 'contain'
            }}
            imageSource={require('./01-speaking-004.jpg')}
        />

        <Section
            headerText={
                <Text>
                    <BasicText content="Have " />
                    <Text style={styles.textEmphasis}>light </Text>
                    <BasicText content="on your face and not in the listener's eyes." />
                </Text>
            }
            imageStyle={{
                resizeMode: 'contain'
            }}
            imageSource={require('./01-speaking-005.jpg')}
        />

        <Section
            headerText={
                <Text>
                    <BasicText content="Keep your " />
                    <Text style={styles.textEmphasis}>hands </Text>
                    <BasicText content="away from your face." />
                </Text>
            }
            imageStyle={{
                resizeMode: 'contain'
            }}
            imageSource={require('./01-speaking-006.jpg')}
        />

        <Section
            headerText={
                <Text>
                    <BasicText content="If you're not understood, say the same thing " />
                    <Text style={styles.textEmphasis}>differently </Text>
                    <BasicText content="- don't just repeat it." />
                </Text>
            }
            imageSource={require('./01-speaking-007.jpg')}
        />

        <Section
            headerText={
                <Text>
                    <Text>Be </Text>
                    <Text style={{fontWeight: 'bold'}}>patient</Text>
                    <Text>: understanding speech may be difficult even with a hearing aid.</Text>
                </Text>
            }
            imageSource={require('./01-speaking-008.jpg')}
        />

        <Section
            headerText={
                <Text>
                    <Text>Effective communication will make life easier for both the </Text>
                    <Text style={{fontWeight: 'bold'}}>listener and you.</Text>
                </Text>
            }
            imageSource={require('./01-speaking-010.png')}
            imageStyle={{
                resizeMode: 'center',
                borderWidth: 0,
            }}
        />

        <SimpleSection text={
            <Text>
                <Text>Remember, </Text>
                <Text style={{fontWeight: 'bold'}}>practice </Text>
                <Text>makes perfect!</Text>
            </Text>
        } />

      </DefaultSwipePage>
    );
  }
}
