import React, { Component } from 'react';
import { Text, ScrollView, View } from 'react-native';
import Washing from './Washing';
import RemoveWax from './RemoveWax';
import { Section, SimpleSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import Button from '../../common/section_button'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

class Types extends Component {
  static navigationOptions = {
    title: 'Cleaning and washing a BTE aid mould',
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <DefaultSwipePage title="Cleaning and washing a BTE aid mould">

        <SimpleSection>
            <View style={{flex: 1}}>
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <BasicText
                        textStyle={{fontSize: 13}}
                        content="To remove wax or moisture with air blower which cannot be removed with a wax brush or cleaning wire" />
                    </View>
                    <View style={{flex: 1}}>
                        <Button onPress={() => navigate('RemoveWax')} title="Removing wax instructions" />
                    </View>
                </View>
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <BasicText
                         textStyle={{fontSize: 13}}
                        content="Washing BTE moulds and tubing to remove wax which cannot be removed with a wax brush, wire cleaning tool or air blower" />
                    </View>
                    <View style={{flex: 1, marginBottom: 10}}>
                        <Button onPress={() => navigate('Washing')} title="Washing wax instructions" />
                    </View>
                </View>
                <View style={{flex: 1}}>
                    <Text style={{textAlign: 'left', color: "lightgray"}}>
                        <Text>Note:</Text>
                        <Text>{"\n"}</Text>
                        Frequent removal of wax from BTE aid moulds or ITE aids may indicate wax build up in
                        the ear canal. Arrange to have the ear checked by a registered nurse or medical
                        practitioner and action taken if required to clear excess wax.
                    </Text>
                </View>
            </View>
        </SimpleSection>

      </DefaultSwipePage>
    );
  }
}

export let Children = {
  Washing: { screen: Washing },
  RemoveWax: { screen: RemoveWax }
};
export default Types;
