import React, { Component } from 'react';
import { Text, View, ScrollView, Image } from 'react-native';

import { DenseSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class BehindTheEar extends Component {
  static navigationOptions = {
    title: 'Removing wax or moisture',
  };

  render() {
    return (
      <DefaultSwipePage title="Removing wax or moisture">

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text style={styles.denseSectionHeader}>Step one</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                Remove mould from hearing aid by pulling the tubing away from the earhook.
                            </Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                    <View style={{flex: 2, alignItems: 'center'}}>
                        <Image style={styles.image} style={styles.image} source={require('./07-cleaning-034.jpg')} />
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                To avoid possible damage, hold the tubing and the earhook, not the mould
                                 and hearing aid.
                            </Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                </View>
            }
        />

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text style={styles.denseSectionHeader}>Step two</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                Insert nozzle of air blower into sound or air hole in mould or open end
                                 of tubing. Blow out wax or moisture thoroughly.
                            </Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                    <View style={{flex: 2, alignItems: 'center'}}>
                        <Image style={styles.image} style={styles.image} source={require('./07-cleaning-033.jpg')} />
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                Air blowers can be purchased from hearing service providers.
                            </Text>
                        </Text>
                    </View>
                </View>
            }
        />

       <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text style={styles.denseSectionHeader}>Step three</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                Push the tubing of the mould over the end of the earhook with mould
                                facing correctly to fit in the ear.
                            </Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                    <View style={{flex: 2, alignItems: 'center', marginBottom: 20}}>
                        <Image style={styles.image} style={styles.image} source={require('./07-cleaning-030.jpg')} />
                    </View>
                    <View style={{flex: 1}} />
                </View>
            }
        />

      </DefaultSwipePage>
    );
  }
}
