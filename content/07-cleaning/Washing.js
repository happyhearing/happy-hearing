import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';

import { DenseSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class InTheEar extends Component {
  static navigationOptions = {
    title: 'Washing BTE aid moulds and tubing',
  };
  render() {
    return (
      <DefaultSwipePage title="Washing BTE aid moulds and tubing">

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text style={styles.denseSectionHeader}>Step one</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                Remove mould from hearing aid by pulling tubing away from the earhook.
                            </Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                    <View style={{flex: 2, alignItems: 'center'}}>
                        <Image style={styles.image} style={styles.image} source={require('./07-cleaning-034.jpg')} />
                    </View>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                To avoid possible damage, hold the tubing and the earhook, not the
                                mould and hearing aid.
                            </Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                </View>
            }
        />

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <Text style={styles.denseSectionText}>
                        <Text style={styles.denseSectionHeader}>Step two</Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Place mould and tubing in a cup of warm (not hot) water and soak until
                            wax or dirt is soft.
                        </Text>
                    </Text>
                </View>
            }
        />

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <Text style={styles.denseSectionText}>
                        <Text style={styles.denseSectionHeader}>Step three</Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Flow warm tap water through the tubing until the tubing and mould are
                            clear of wax or dirt.
                        </Text>
                    </Text>
                </View>
            }
        />

       <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text style={styles.denseSectionHeader}>Step four</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                Dry the mould by shaking and wiping with a tissue. Desirably insert nozzle
                                of air blower into sound or air hole or open end of tubing. Blowout any wax or moisture thoroughly.
                            </Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                    <View style={{flex: 2, alignItems: 'center', marginBottom: 20}}>
                        <Image style={styles.image} style={styles.image} source={require('./07-cleaning-033.jpg')} />
                    </View>
                </View>
            }
        />

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <Text style={styles.denseSectionText}>
                        <Text style={styles.denseSectionHeader}>Step five</Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            If an air blower is not available thoroughly dry mould and tubing in a
                            secure place overnight.
                        </Text>
                    </Text>
                </View>
            }
        />

       <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text style={styles.denseSectionHeader}>Step six</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                Replace mould by pushing tubing back over the earhook.
                            </Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                    <View style={{flex: 2, alignItems: 'center'}}>
                        <Image style={styles.image} style={styles.image} source={require('./07-cleaning-031.jpg')} />
                    </View>
                    <View style={{flex: 1, marginBottom: 20}}>
                        <Text style={styles.denseSectionText}>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                Frequent removal of wax from BTE aid moulds or ITE aids may indicate wax
                                build up in the ear canal. Arrange to have the ear checked by a
                                regiatered nurse or medical practitioner and action taken if needed to
                                clear excess wax.
                            </Text>
                        </Text>
                    </View>
                </View>
            }
        />

      </DefaultSwipePage>
    );
  }
}
