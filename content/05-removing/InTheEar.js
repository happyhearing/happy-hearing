import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';

import { DenseSection, Section, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class InTheEar extends Component {
  static navigationOptions = {
    title: 'Removing ITE aids',
  };
  render() {
    return (
      <DefaultSwipePage title="Removing ITE aids">

        <DenseSection
            content={
                <View style={{flex: 2}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text style={styles.denseSectionHeader}>Step one</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                Gently remove an ITE aid by pulling on the removal line, if there is
                                 one, and lifting it up as you take it out.
                            </Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                    <View style={{flex: 1, alignItems: 'center'}}>
                        <Image style={styles.image} source={require('./05-removing-026.jpg')} />
                    </View>
                    <View style={{flex: 1}} />
                </View>
            }
        />

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <Text style={styles.denseSectionText}>
                        <Text style={styles.denseSectionHeader}>Step two</Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Place aid in storage container ensuring battery holder is partly open
                            and battery remains in the holder.
                        </Text>
                        <Text>{"\n\n"}</Text>
                        <Text>
                            Place a drying capsule (available from a hearing services provider) in
                            the jar if moisture accumulation in the mould, tubing or aid is a problem.
                            {"\n"}
                            After a few months watch for signs that drying capsule needs replacement
                            e.g. it may change colour.
                        </Text>
                    </Text>
                </View>
            }
        />

      </DefaultSwipePage>
    );
  }
}
