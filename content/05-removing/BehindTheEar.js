import React, { Component } from 'react';
import { Text, View, ScrollView, Image } from 'react-native';

import { DenseSection, MultiLineText, BasicText } from '../../common/content_pages/swipe_component'
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import styles from '../../common/styles';

export default class BehindTheEar extends Component {
  static navigationOptions = {
    title: 'Removing BTE aids',
  };

  render() {
    return (
      <DefaultSwipePage title="Removing BTE aids">

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 2}}>
                        <Text style={styles.denseSectionText}>
                            <Text style={styles.denseSectionHeader}>Step one</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                Before removing a BTE aids’s mould, turn the aid gently forward and lift it
                                slightly clear of ear, then withdraw the mould.
                            </Text>
                            <Text>{"\n\n"}</Text>
                            <Text>
                               To remove, pull the mould rather than the tubing.
                            </Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                    <View style={{flex: 2, alignItems: 'center'}}>
                        <Image style={styles.image} source={require('./05-removing-027.jpg')} />
                    </View>
                    <View style={{flex: 1}} />
                </View>
            }
        />

        <DenseSection
            content={
                <View style={{flex: 1}}>
                    <View style={{flex: 1}}>
                        <Text style={styles.denseSectionText}>
                            <Text style={styles.denseSectionHeader}>Step two</Text>
                            <Text>{"\n\n"}</Text>
                            <Text>
                                Place aid in storage container ensuring battery holder is partly open
                                and battery remains in the holder.
                            </Text>
                            <Text>{"\n\n"}</Text>
                        </Text>
                    </View>
                    <View style={{flex: 2, paddingLeft: 30, paddingRight: 30, alignItems: 'center'}}>
                        <Image style={styles.image} style={styles.image} source={require('./05-removing-025.jpg')} />
                    </View>
                    <View style={{flex: 1}}>
                        <Text>{"\n"}</Text>
                        <Text style={styles.denseSectionText}>
                           Place a drying capsule (available from a hearing services provider) in the jar
                           if moisture accumulation in the mould, tubing or aid is a problem.
                           {"\n"}
                           After a few months watch for signs that drying capsule needs replacement
                           e.g. it may change colour.
                        </Text>
                    </View>
                </View>
            }
        />

      </DefaultSwipePage>
    );
  }
}
