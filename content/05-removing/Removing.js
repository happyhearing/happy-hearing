import React, { Component } from 'react';
import { Text, ScrollView, View, Button } from 'react-native';
import InTheEar from './InTheEar';
import BehindTheEar from './BehindTheEar';
import DefaultSwipePage from '../../common/content_pages/default_swipe_page'
import MenuButton from '../../common/menu_button';
import styles from '../../common/styles';

class Types extends Component {
  static navigationOptions = {
    title: 'Removing and storing aids',
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <ScrollView>
        <MenuButton text="Removing and storing Behind The Ear aids" onPress={() => navigate('RemovBehindTheEar')} />
        <MenuButton text="Removing and storing In The Ear aids" onPress={() => navigate('RemovInTheEar')} />
      </ScrollView>
    );
  }
}

export let Children = {
  RemovInTheEar: { screen: InTheEar },
  RemovBehindTheEar: { screen: BehindTheEar }
};
export default Types;
