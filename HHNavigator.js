import React, { Component } from 'react';
import { Button, ScrollView, View, Text, Image, Animated } from 'react-native';
import { StackNavigator } from 'react-navigation';
import styles from './common/styles';
import MenuButton from './common/menu_button';

import Speaking from './content/01-speaking/Speaking';
import Types, { Children as TypesChildren } from './content/02-types/Types';
import Controls, { Children as ControlsChildren } from './content/03-controls/Controls';
import Inserting, { Children as InsertingChildren } from './content/04-inserting/Inserting';
import Removing, { Children as RemovingChildren } from './content/05-removing/Removing';
import Changing from './content/06-changing/Changing';
import Cleaning, { Children as CleaningChildren } from './content/07-cleaning/Cleaning';
import Attaching, { Children as AttachingChildren } from './content/08-attaching/Attaching';
import Troubleshooting, { Children as TroubleshootingChildren } from './content/09-troubleshooting/Troubleshooting';
import HearingLoss from './content/10-hearing-loss/HearingLoss';
import FurtherInfo from './content/11-further-info/FurtherInfo';
import Disclaimer from './content/12-disclaimer/Disclaimer';

class MenuScreen extends React.Component {
  static navigationOptions = {
    title: 'Home Menu',
  };

    state = {
      buttonFadeAnim: new Animated.Value(0),  // Initial value for opacity: 0
    }

    componentDidMount() {
      Animated.timing(
        this.state.buttonFadeAnim,
        {
          toValue: 1,
          duration: 500,
        }
      ).start();
    }

  render() {
    let { buttonFadeAnim } = this.state;
    const { navigate } = this.props.navigation;
    return (
      <ScrollView>
        <View style={{backgroundColor: '#1F7470', alignItems: 'center', padding: 10}}>
            <Image style={styles.logo} source={require('./images/logo_yellow.png')} />
            <Text style={{fontWeight: 'bold', fontSize: 30, color: '#FFF', textShadowColor: '#000', textShadowOffset: {width: 2, height: 2}, textShadowRadius: 5}}>
            H<Text style={{color: '#f6e313'}}>app</Text>y Hearing
            </Text>
        </View>
        <View style={{backgroundColor: '#1F7470', alignItems: 'center', padding: 10, paddingTop: 0, paddingBottom: 20}}>
            <Text style={{textAlign: 'center', color: '#FFF', fontWeight: 'bold'}}>
                Assisting people to use their hearing aids
            </Text>
            <Text />
            <Text style={{textAlign: 'center', color: '#FFF'}}>
                For nurses, aged care staff, family and volunteers to assist those they care for to
                manage their hearing aids and thus enhance their quality of life and health.
            </Text>
        </View>
        <Animated.View style={{...this.props.style, opacity: buttonFadeAnim}}>
            <MenuButton text={Speaking.navigationOptions.title} onPress={() => navigate('Speaking')} />
            <MenuButton text={Types.navigationOptions.title} onPress={() => navigate('Types')} />
            <MenuButton text={Controls.navigationOptions.title} onPress={() => navigate('Controls')} />
            <MenuButton text={Inserting.navigationOptions.title} onPress={() => navigate('Inserting')} />
            <MenuButton text={Removing.navigationOptions.title} onPress={() => navigate('Removing')} />
            <MenuButton text={Changing.navigationOptions.title} onPress={() => navigate('Changing')} />
            <MenuButton text={Cleaning.navigationOptions.title} onPress={() => navigate('Cleaning')} />
            <MenuButton text={Attaching.navigationOptions.title} onPress={() => navigate('Attaching')} />
            <MenuButton text={Troubleshooting.navigationOptions.title} onPress={() => navigate('Troubleshooting')} />
            <MenuButton text={HearingLoss.navigationOptions.title} onPress={() => navigate('HearingLoss')} />
            <MenuButton text={FurtherInfo.navigationOptions.title} onPress={() => navigate('FurtherInfo')} />
            <MenuButton text={Disclaimer.navigationOptions.title} onPress={() => navigate('Disclaimer')} />
            <View style={{padding: 10, flex: 1, justifyContent: 'center', backgroundColor: "#464646", borderTopWidth: 2, borderTopColor: "#ccc"}}>
                <Text style={{textAlign: 'center', color: "#FFF", fontSize: 12}}>
                    An initiative of the LinkAGE Program of St Vincent’s Care Services in conjunction with Deafness Forum of Australia and Australian Hearing.
                </Text>
                <Text style={{textAlign: 'center', color: "#FFF", fontSize: 12}}>
                    Produced by corporate volunteers from Atlassian.
                </Text>
                <Text style={{textAlign: 'center', color: "#FFF", fontSize: 10, paddingTop: 5}}>
                    Illustrations and photos by courtesy of Australian Hearing and Deafness Forum of Australia
                </Text>
                <Text style={{textAlign: 'center', color: "#FFF", fontSize: 10}}>
                    © Deafness Forum of Australia
                </Text>
            </View>
        </Animated.View>
      </ScrollView>
    );
  }
}

const HHNavigator = StackNavigator({
  Menu: { screen: MenuScreen},
  Speaking: { screen: Speaking },
  Types: { screen: Types },
  ...TypesChildren,
  Controls: { screen: Controls },
  ...ControlsChildren,
  Inserting: { screen: Inserting },
  ...InsertingChildren,
  Removing: { screen: Removing },
  ...RemovingChildren,
  Changing: { screen: Changing },
  Cleaning: { screen: Cleaning },
  ...CleaningChildren,
  Attaching: { screen: Attaching },
  ...AttachingChildren,
  Troubleshooting: { screen: Troubleshooting },
  ...TroubleshootingChildren,
  HearingLoss: { screen: HearingLoss },
  FurtherInfo: { screen: FurtherInfo },
  Disclaimer: { screen: Disclaimer }
},
{
    headerMode: "float",
    navigationOptions: {
        headerTintColor: 'blue'
    }
}
);

export default HHNavigator;
